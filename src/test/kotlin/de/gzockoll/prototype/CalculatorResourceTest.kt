package de.gzockoll.prototype

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class CalculatorResourceTest {
    lateinit var cut: CalendarResource

    @Before
    fun setUp() {
        cut = CalendarResource()
    }

    @Test
    fun testCalculateForwardDaysByTime() {
        assertThat(cut.calculateForwardDays(LocalTime.parse("00:00"))).isEqualTo(1)
        assertThat(cut.calculateForwardDays(cut.TURN_OVER_TIME.minusNanos(1))).isEqualTo(1)
        assertThat(cut.calculateForwardDays(cut.TURN_OVER_TIME)).isEqualTo(2)
        assertThat(cut.calculateForwardDays(LocalTime.parse("23:59"))).isEqualTo(2)
    }

    @Test
    fun testCalculateForwardDaysByDateTime() {
        assertThat(cut.calculateForwardDays(LocalTime.parse("00:00"))).isEqualTo(1)
        assertThat(cut.calculateForwardDays(cut.TURN_OVER_TIME.minusNanos(1))).isEqualTo(1)
        assertThat(cut.calculateForwardDays(cut.TURN_OVER_TIME)).isEqualTo(2)
        assertThat(cut.calculateForwardDays(LocalTime.parse("23:59"))).isEqualTo(2)
    }

    @Test
    fun testGetWorkingDaysThursdayBeforeTurnOver() {
        val before = LocalDateTime.parse("2017-03-16T10:11:30")
        val result = cut.workingdays(before)

        assertThat(result.first()).isEqualTo(LocalDate.parse("2017-03-17"))
        assertThat(result.max()).isEqualTo(LocalDate.parse("2017-06-14"))
    }

    @Test
    fun testGetWorkingDaysThursdayAfterTurnOver() {
        val before = LocalDateTime.parse("2017-03-16T20:11:30")
        val result = cut.workingdays(before)

        assertThat(result.first()).isEqualTo(LocalDate.parse("2017-03-18"))
        assertThat(result.max()).isEqualTo(LocalDate.parse("2017-06-14"))
    }

    @Test
    fun testGetWorkingDaysFridayBeforeTurnOver() {
        val before = LocalDateTime.parse("2017-03-17T10:11:30")
        val result = cut.workingdays(before)

        assertThat(result.first()).isEqualTo(LocalDate.parse("2017-03-18"))
        assertThat(result.max()).isEqualTo(LocalDate.parse("2017-06-15"))
    }

    @Test
    fun testGetWorkingDaysFridayAfterTurnOver() {
        val before = LocalDateTime.parse("2017-03-17T20:11:30")
        val result = cut.workingdays(before)

        assertThat(result.first()).isEqualTo(LocalDate.parse("2017-03-20"))
        assertThat(result.max()).isEqualTo(LocalDate.parse("2017-06-15"))
    }

    @Test
    fun testGetWorkingDaysSaturdayBeforeTurnOver() {
        val before = LocalDateTime.parse("2017-03-18T10:11:30")
        val result = cut.workingdays(before)

        assertThat(result.first()).isEqualTo(LocalDate.parse("2017-03-20"))
        assertThat(result.max()).isEqualTo(LocalDate.parse("2017-06-16"))
    }

    @Test
    fun testGetWorkingDaysSaturdayAfterTurnOver() {
        val before = LocalDateTime.parse("2017-03-18T20:11:30")
        val result = cut.workingdays(before)

        assertThat(result.first()).isEqualTo(LocalDate.parse("2017-03-21"))
        assertThat(result.max()).isEqualTo(LocalDate.parse("2017-06-16"))
    }
}