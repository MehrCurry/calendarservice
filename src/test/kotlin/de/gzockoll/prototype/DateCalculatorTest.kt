package de.gzockoll.prototype

import de.jollyday.HolidayCalendar
import de.jollyday.HolidayManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import java.time.DayOfWeek
import java.time.LocalDate

class DateCalculatorTest {
    lateinit var cut: DateCalculator

    @Before
    fun setUp() {
        cut = DateCalculator(WorkingWeek.withWeekend(DayOfWeek.SUNDAY), HolidayManager.getInstance(HolidayCalendar.GERMANY).getHolidays(2017).map { it.date }.toSet())
    }

    @Test
    fun testIsWorkingday() {
        assertThat(cut.isWorkingDay(LocalDate.parse("2017-03-17"))).isTrue()
        assertThat(cut.isWorkingDay(LocalDate.parse("2017-03-18"))).isTrue()
        assertThat(cut.isWorkingDay(LocalDate.parse("2017-03-19"))).isFalse()
    }

    @Test
    fun testIsWorkingdayEesterWeekend() {
        val holyThursday = LocalDate.parse("2017-04-13")
        assertThat(cut.isWorkingDay(holyThursday)).isTrue()
        assertThat(cut.isWorkingDay(holyThursday.plusDays(1))).isFalse()
        assertThat(cut.isWorkingDay(holyThursday.plusDays(2))).isTrue()
        assertThat(cut.isWorkingDay(holyThursday.plusDays(3))).isFalse()
        assertThat(cut.isWorkingDay(holyThursday.plusDays(4))).isFalse()
        assertThat(cut.isWorkingDay(holyThursday.plusDays(5))).isTrue()
    }

    @Test
    fun testNextWorkingDay() {
        val holyThursday = LocalDate.parse("2017-04-13")
        assertThat(cut.nextWorkingDay(holyThursday, 0)).isEqualTo(holyThursday)
        assertThat(cut.nextWorkingDay(holyThursday, 1)).isEqualTo(holyThursday.plusDays(2))
        assertThat(cut.nextWorkingDay(holyThursday, 2)).isEqualTo(holyThursday.plusDays(5))
    }
}