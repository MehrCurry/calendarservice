package de.gzockoll.prototype

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.DayOfWeek

class WorkingWeekTest {
    @Test
    fun testWorkingWeekByWorkingDays() {
        val ww = WorkingWeek.of(DayOfWeek.values().toSet().minus(DayOfWeek.SUNDAY))
        assertThat(ww.isWorkingDay(DayOfWeek.MONDAY)).isTrue()
        assertThat(ww.isWorkingDay(DayOfWeek.FRIDAY)).isTrue()
        assertThat(ww.isWorkingDay(DayOfWeek.SATURDAY)).isTrue()
        assertThat(ww.isWorkingDay(DayOfWeek.SUNDAY)).isFalse()
    }

    @Test
    fun testWorkingWeekByWeekendDays() {
        val ww = WorkingWeek.withWeekend(DayOfWeek.SUNDAY)
        assertThat(ww.isWorkingDay(DayOfWeek.MONDAY)).isTrue()
        assertThat(ww.isWorkingDay(DayOfWeek.FRIDAY)).isTrue()
        assertThat(ww.isWorkingDay(DayOfWeek.SATURDAY)).isTrue()
        assertThat(ww.isWorkingDay(DayOfWeek.SUNDAY)).isFalse()
    }
}