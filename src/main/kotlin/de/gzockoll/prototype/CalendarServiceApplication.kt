package de.gzockoll.prototype

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class CalendarServiceApplication

fun main(args: Array<String>) {
    SpringApplication.run(CalendarServiceApplication::class.java, *args)
}
