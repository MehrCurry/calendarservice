package de.gzockoll.prototype

import java.time.LocalDate

class DateCalculator(val workingWeek: WorkingWeek, val holidays: Collection<LocalDate>) {
    fun isWorkingDay(date: LocalDate): Boolean {
        return !holidays.contains(date) && workingWeek.isWorkingDay(date)
    }

    fun nextWorkingDay(date: LocalDate, offset: Int): LocalDate {
        val days = generateSequence(date) { it.plusDays(1) }
        return days.filter { isWorkingDay(it) }.take(offset + 1).toList().last()
    }

    fun getWorkingDaysUntil(startDate: LocalDate, condition: (d: LocalDate) -> Boolean): Iterable<LocalDate> {
        val generator = generateSequence(startDate) { it.plusDays(1) }
        return generator.takeWhile(condition).filter { isWorkingDay(it) }.toList()
    }
}