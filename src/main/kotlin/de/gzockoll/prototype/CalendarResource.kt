package de.gzockoll.prototype

import de.jollyday.HolidayCalendar
import de.jollyday.HolidayManager
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.TemporalAdjusters

@RestController
@RequestMapping("/calendar")
class CalendarResource {
    val TURN_OVER_TIME: LocalTime = LocalTime.parse("20:00")

    lateinit var dateCalculator: DateCalculator

    init {
        val start = LocalDate.now().with(TemporalAdjusters.firstDayOfYear())

        val holidays = HolidayManager.getInstance(HolidayCalendar.GERMANY).getHolidays(start, start.plusYears(5)).map { it.date }.toSet()
        val workingWeek = WorkingWeek.withWeekend(DayOfWeek.SUNDAY)
        dateCalculator = DateCalculator(workingWeek, holidays)
    }

    @RequestMapping(path = arrayOf("/workingdays"), method = arrayOf(RequestMethod.GET))
    fun workingdays(): Iterable<LocalDate> = workingdays(LocalDateTime.now())

    fun workingdays(pointInTime: LocalDateTime): Iterable<LocalDate> {
        val forwardDays = calculateForwardDays(pointInTime)
        val startDate = dateCalculator.nextWorkingDay(pointInTime.toLocalDate(), forwardDays)
        val endDate = pointInTime.toLocalDate().plusDays(90)

        return dateCalculator.getWorkingDaysUntil(startDate, { !it.isAfter(endDate) })
    }

    fun calculateForwardDays(dateTime: LocalDateTime): Int = calculateForwardDays(dateTime.toLocalTime())

    fun calculateForwardDays(time: LocalTime): Int = if (time.isBefore(TURN_OVER_TIME)) 1 else 2
}