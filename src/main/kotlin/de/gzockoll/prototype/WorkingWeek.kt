package de.gzockoll.prototype

import java.time.DayOfWeek
import java.time.LocalDate

class WorkingWeek private constructor(val days: Collection<DayOfWeek>) {
    fun isWorkingDay(day: DayOfWeek): Boolean {
        return days.contains(day)
    }

    companion object {
        fun of(vararg days: DayOfWeek): WorkingWeek {
            return WorkingWeek(days.toSet())
        }

        fun of(days: Collection<DayOfWeek>): WorkingWeek {
            return WorkingWeek(days)
        }

        fun withWeekend(vararg days: DayOfWeek): WorkingWeek {
            val fullWeek = DayOfWeek.values().toSet()
            val workingDays = fullWeek.minus(days)
            return WorkingWeek(workingDays)
        }
    }

    fun isWorkingDay(date: LocalDate) = isWorkingDay(date.dayOfWeek)
}